function distance(first, second){
	if(!(first instanceof Array) || !(second instanceof Array)){
		throw new Error('InvalidType');
	}
	else{
		var duplicates = 0;
		var f = new Set(first);
		var s = new Set(second);
		first = [...f];
		second=[...s];
		for(var i=0;i<first.length;i++){
			for(var j=0; j<second.length;j++){
				if(first[i]===second[j]){
					duplicates++;
				}
			}
		}
		var dist=(first.length+second.length)-(2*duplicates);

		if(first.length==0 && second.length==0){
			return 0;
		}
		
		return dist;
	}
}


module.exports.distance = distance